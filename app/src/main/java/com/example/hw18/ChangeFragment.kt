package com.example.hw18

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import kotlinx.coroutines.*
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.hw18.databinding.FragmentChangeBinding
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ChangeFragment : Fragment() {
    private lateinit var binding: FragmentChangeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChangeBinding.inflate(inflater)
        binding.messageView.setText(MessageInfo.message)
        binding.titleView.setText(MessageInfo.title)
        binding.cancel.setOnClickListener {
            binding.root.findNavController().navigate(R.id.action_changeFragment_to_homeFragment)
        }

        binding.post.setOnClickListener {
            MessageInfo.title = binding.titleView.text.toString()
            MessageInfo.message = binding.messageView.text.toString()
            urlEncoded()
            binding.root.findNavController().navigate(R.id.action_changeFragment_to_homeFragment)
        }
        return binding.root
    }

    private fun urlEncoded() {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .build()

        val service = retrofit.create(ApiInterface::class.java)

        CoroutineScope(Dispatchers.IO).launch {
            val response = service.pushPost(
                MessageInfo.userId,
                MessageInfo.id,
                MessageInfo.title,
                MessageInfo.message
            )
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Toast.makeText(requireContext(), "Successfully posted", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = ChangeFragment()
    }
}