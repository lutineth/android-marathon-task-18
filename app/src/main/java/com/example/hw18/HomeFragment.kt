package com.example.hw18

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.*
import retrofit2.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hw18.databinding.FragmentHomeBinding
import retrofit2.converter.gson.GsonConverterFactory

class HomeFragment : Fragment() {
    private val baseUrl = "https://jsonplaceholder.typicode.com/"
    lateinit var binding: FragmentHomeBinding
    lateinit var messageAdapter: MessageAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        linearLayoutManager = LinearLayoutManager(requireContext())
        binding.recView.layoutManager = linearLayoutManager
        getMyData()
    }

    private fun getMyData() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build()
            .create(ApiInterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<DataItem>?> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<List<DataItem>?>?,
                response: Response<List<DataItem>?>?
            ) {
                val responseBody = response?.body()!!
                messageAdapter = MessageAdapter(responseBody)
                messageAdapter.notifyDataSetChanged()
                binding.recView.adapter = messageAdapter
            }

            override fun onFailure(call: Call<List<DataItem>?>?, t: Throwable?) {
                if (t != null) {
                    Log.d("Main","onFailure: " + t.message)
                }
            }
    })
}
    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}