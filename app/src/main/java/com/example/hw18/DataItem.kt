package com.example.hw18

data class DataItem(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
)