package com.example.hw18

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiInterface {
    @GET("posts")
    fun getData(): Call<List<DataItem>>

    @FormUrlEncoded
    @POST("posts")
    suspend fun pushPost(
        @Field("userId") userId:Int,
        @Field("id") id:Int,
        @Field("title") title:String,
        @Field("body") body:String
    ): Response<DataItem>
}