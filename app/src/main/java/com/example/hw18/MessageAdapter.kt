package com.example.hw18

import android.view.*
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView

class MessageAdapter (private val userList:List<DataItem>): RecyclerView.Adapter<MessageAdapter.ViewHolder>(){
    class ViewHolder (itemView: View):RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.messageTitle)
        var id: TextView = itemView.findViewById(R.id.numberOfMessage)
        var card: CardView = itemView.findViewById(R.id.card)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_items, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = userList[position].title
        holder.id.text = userList[position].id.toString()
        holder.card.setOnClickListener{
            MessageInfo.id = userList[position].id
            MessageInfo.userId = userList[position].userId
            MessageInfo.title = userList[position].title
            MessageInfo.message = userList[position].body
            holder.itemView.findNavController().navigate(R.id.action_homeFragment_to_changeFragment)
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }
}