package com.example.hw18

object MessageInfo
{
    var id:Int = 0
    var userId:Int = 0
    var title:String = ""
    var message:String = ""
}